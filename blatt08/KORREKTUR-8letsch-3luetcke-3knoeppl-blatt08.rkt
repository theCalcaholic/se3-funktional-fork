#lang racket

(require "setkarten-module.rkt")
(require plai) ; for unit testing

;KOMMETAR: 40 / 53 Punkte

;;;;;;;;; AUFGABE 1: Funktionen höherer Ordnung und Closures ;;;;;;;;;;;;;;;;;;;
;KOMMENTAR: 7 Pkt
; 1.1 Wann ist eine Racket-Funktion eine Funktion höherer Ordnung?
; Eine Funktion wird Funktion höherer Ordnung genannt (FHO), wenn sie
; andere Funktionen als Parameter erwartet.
;KOMMENTAR: Was ist mit lambda? Nicht vollständig

;KOMMENTAR: kopf-oder-schwanz ist eine FHO!
; 1.2 Welche der folgenden Funktionen sind FHO und warum?
; a) foldl: JA. foldl nimmt eine Funktion und eine Liste als Parameter an und
;    "faltet" die Liste, indem die Funktion paarweise auf alle Elemente angewendet wird.
; b) kopf-oder-schwanz: NEIN. Die Funktion hat nur einen Parameter, welcher eine Zahl ist.
; c) pimento: JA. Fie Funktion pimento macht das gleiche wie rackets eigene curry Funktion.
;    Hierbei wird ein Parameter an eine Funktion gebunden und eine neue Funktion zurückgegeben.
; d) my-tan: NEIN. Der Parameter x erwartet Zahlen.
;
; 1.3 ((pimento * 5) 7)
; Der innere Aufruf (pimento * 5) wird zuerst ausgewertet. Hierbei wird eine Closure
; erzeugt, in deren Umgebung arg1 gebunden wird. Die Closure hat weiterhin einen
; Verweis auf diese Umgebung, so dass beim weiteren Aufruf der Closure mit dem nächsten
; Parameter 7 auch noch der Wert des ersten Parameters zur Verfügung steht. So können
; Parameter für spätere Aufrufe gebunden werden, ohne dass dies an der Closure von
; außen ersichtlich ist.
;
; 1.4 Zu welchem Wert evaluieren die folgenden Ausdrücke?
; > (foldl (curry * 2) 1 '(1 1 2 3))
; -> 96
; (curry * 2) gibt eine neue Funktion (die Verdopplungsfunktion) zurück.
; foldl wendet diese Verdopplungsfunktion auf alle Elemente der Liste an und reduziert
; dies auf einen Ergebniswert, zusätzlich wird noch mit dem Wert 1 multipliziert.
; Also: (* (* (* (* (* 2 1) 2 1) 2 2) 2 3) 1)
;
; > (map cons '(1 2 3 4) '(1 2 3 4))
; -> '((1 . 1) (2 . 2) (3 . 3) (4 . 4))
; map wendet eine Funktion (hier cons) auf jedes Element einer Liste, oder
; mehrere Elemente mehrere Listen an. Hier sind zwei Listen gleicher Länge übergeben,
; also werden die Elemente paarweise als Parameter an cons gebunden und somit
; vier gepunktete Paare erzeugt.
;
; > (filter pair? '((a b ) () 1 (())))
; -> '((a b) (()))
; filter nimmt eine Funktion und eine Liste als Parameter an.
; Die Funktion wird als Prädikat verwendet wird und eine Liste aller Elemente
; der übergebenen Liste zurückgegeben, die das Prädikat erfüllen. pair? überprüft
; ob das Element ein Paar ist. In Racket sind alle nicht leeren Listen auch Paare,
; daher fallen nur die leere Liste und die 1 aus der Liste heraus.
;
; > (map (compose (curry + 33) (curry * 1.8)) '(5505 100 0 1 1000 -273.15))
; --> '(9942.0 213.0 33 34.8 1833.0 -458.66999999999996)
; (curry + 33) und (curry * 1.8) erzeugen zwei Funktionen. Die erste inkrementiert
; einen Wert um 33, die andere mulipliziert mit dem Faktor 1.8. (compose ...) erzeugt
; eine neue Funktion als Hintereinanderausführung dieser beiden Funktionen, wobei
; zuerst multipliziert und dann inkrementiert wird. (map ...) wendet dann diese
; Funktion auf alle Elemente einer Liste an.

;;;;;;;;; AUFGABE 2: Einfache funktionale Ausdrücke höherer Ordnung ;;;;;;;;;;;;

; KOMMENTAR: Schön 9 Pkt

; 2.1
; Berechnet die Liste der Absolutbeträge aller Zahlen in einer Liste
; Aufruf:
; > (abs-list '(-1 1 2 3 -42))
; '(1 1 2 3 42)
(define (abs-list xs)
  (map abs
       xs))

; 2.2
; Konstruiert die Teilliste von xs die die durch 3 teilbaren Zahlen enthält.
; Aufruf:
; > (dividable-by-3-list '(1 2 3 4 5 6))
; '(3 6)
(define (dividable-by-3-list xs)
  (filter (lambda (x)
            (= 0 (modulo x 3)))
          xs))

; 2.3
; Bildet die Summe aller gerade Zahlen größer 10 aus einer Liste.
; Aufruf:
; > (sum-of-even-numbers-larger-ten '(2 3 4 12 13 14))
; 26
(define (sum-of-even-numbers-larger-ten xs)
  (foldl +
         0
         (filter (lambda (x)
                   (and (< 10 x) (even? x)))
                 xs)))
; KOMMENTAR: (apply + ...) hätte es auch getan.

; 2.4
; Teilt eine Liste in zwei Teillisten auf an Hand eines Prädikats.
; Aufruf:
; > (split-list '(1 2 3 4 5 6 42) odd?)
; '((1 3 5) (2 4 6 42))
(define (split-list xs praed)
  (list
   (filter praed
            xs)
   (filter (lambda (x)
             (not (praed x)))
           xs)))
; KOMMENTAR: Hier hättet ihr "negate" nutzen können. Oder einfach "partition" :)

;;;;;;;;; AUFGABE 3: Spieltheorie: Das Kartenspiel SET! ;;;;;;;;;;;;;;;;;;;;;;;;

; KOMMENTAR: Schön 24 Pkt

; 3.1
; Mögliche Ausprägungen der Spielkarten werden als Liste modelliert (genau wie 
; wir damals die Schmetterlinge modelliert haben).
; Listen sind gut, um mit Funktionen höherer Ordnung zu arbeiten.
; Außerdem können wir die Menge aller Karten als Kartesiches Produkt der "card-traits"
; mit sich selbst darstellen. Eine einzelne Karte ist dann eine Liste mit vier Elementen
; wobei jedes Element einer der vier Eigenschaften entspricht.
(define card-traits 
  (list
    '(1 2 3) ; count
    '(waves oval rectangle) ; pattern
    '(outline solid hatched) ; fill mode
    '(red green blue) ; colour
))

; 3.2 
; Generate a deck of cards (ordered)
; Call:
; > cards
; '((1 waves outline red)
;   (1 waves outline green)
;   (1 waves outline blue)
;   (1 waves solid red)
;   ...
(define (cards)
  (define (create-deck counts patterns fillmodes colours)
    (append-map (lambda (count) 
           (append-map (lambda (pattern) 
                  (append-map (lambda (fill) 
                         (map (lambda (colour) 
                                (list count pattern fill colour))
                              colours)
                         ) 
                       fillmodes)
                  )
                patterns)
           ) 
         counts) 
    )
  (apply create-deck card-traits)
  )
; KOMMENTAR: Das geht bestimmt noch schöner. :)

; Shows all cards
; Call:
; > (show-cards (cards))
; ... graphical output
(define (show-cards cs)
  (map (lambda (card) (apply show-set-card card))
       cs
  )
)

; 3.3

; Check if three things are either the same or all different from each other.
; Call:
; > (same-or-different 1 1 1)
; #t
(define (same-or-different a b c)
  (or (and (eq? a b) (eq? b c)) ; all the same?
      (and 
        (not (eq? a b))
        (not (eq? b c))
        (not (eq? a c))
      )
  )
)
; KOMMENTAR: Jups.

; unit test
(test (same-or-different 1 1 1) #t)
(test (same-or-different 1 2 3) #t)
(test (same-or-different 1 2 2) #f)

; Checks if a given list of 3 cards is a "set" as according to the game rules
(define (is-a-set? cards)
  (andmap same-or-different (first cards) (second cards) (third cards)))
; KOMMENTAR: Uiii, andmap :)
; KOMMENTAR: Hier hättet ihr auch noch apply nutzen können statt first, second, third...

; TESTS

; set
( is-a-set? 
  '((2 red oval hatched )
    (2 red rectangle hatched)
    (2 red wave hatched)))
; -> #t
                 
; no set          
(is-a-set? 
 '((2 red rectangle outline)
   (2 green rectangle outline )
   (1 green rectangle solid)))
; -> #f

