#lang racket
; 1.
(define (abs-list xs)
  (map abs
       xs))

; 2.
(define (dividable-by-3-list xs)
  (filter (lambda (x)
            (= 0 (modulo x 3)))
          xs))

; 3.
(define (sum-greater-10-and-even xs)
  (foldl +
         0
         (filter (lambda (x)
                   (and (< 10 x) (even? x)))
                 xs)))

; 4.
(define (split-list xs praed)
  (list
   (filter praed
            xs)
   (filter (lambda (x)
             (not (praed x)))
           xs)))