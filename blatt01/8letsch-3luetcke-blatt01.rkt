#lang racket


;;;;;;;;;;;;;;;;;
;; AUFGABE 1.1 ;;
;;;;;;;;;;;;;;;;;


; Converts degrees to radian
;
; Example call:
; > (deg2rad 45)
; > 0.7853981633974483
(define (deg2rad deg) 
  ( * ( / deg 180 ) pi))
 
; Converts degrees to radian
;
; Example call:
; > (rad2deg 0.785)
; > 44.97718691776962
(define (rad2deg rad)
  (* rad (/ 180 pi)))


;;;;;;;;;;;;;;;;;
;; AUFGABE 1.2 ;;
;;;;;;;;;;;;;;;;;


; Calculates the arcostangens function of a given angle
; Infix notation of the calculation: acos(a) = atan(sqrt(1-a^2) / a)
;
; Example call:
; > (acos (cos 1))
; > 0.9999999999999999
(define (acos alpha)
  (atan 
    (/ 
      (sqrt (- 1 (* alpha alpha))) 
      alpha
    )
  )
)


;;;;;;;;;;;;;;;;;
;; AUFGABE 1.3 ;;
;;;;;;;;;;;;;;;;;


; Converts nautic miles to kilometers
;
; Example call:
; > (nm2km 1)
; > 1.852
(define (nm2km nm)
  (* nm 1.852))


;;;;;;;;;;;;;;;;;
;; AUFGABE 2.1 ;;
;;;;;;;;;;;;;;;;;


; Winkel von Ort A zu Ort B, als Hilfsfunktion.
(define (degAB latA latB longA longB)
   (+ 
       (* (sin (deg2rad latA)) (sin (deg2rad latB))) (* (cos (deg2rad latA)) (cos (deg2rad latB)) (cos (- (deg2rad longA) (deg2rad longB))))
      ))

; Berechnet die Distanz zwischen zwei Orten A und B.
;
; lat -> geogr. breite
; long -> geogr. laenge
;
; Anmerkung: °N & °E sind als positive Werte einzutragen, °S & °W hingegen als negative.
;
(define (distanzAB latA latB longA longB)
  (nm2km 
   (*(rad2deg  
     (acos (degAB latA latB longA longB))) 60)))


; Entfernung von Oslo nach Honkong:
(define latO 59.93)
(define latH 22.2)
(define longO 10.75)
(define longH 114.1)

(write "Die Distanz von Oslo nach Honkong in km beträgt: ")
(distanzAB latO latH longO longH)
; = 8589.41221 km


; Entfernung von San Francisco nach Honolulu:
(write "Die Distanz von San Francisco nach Honolulu in km beträgt: ")
(distanzAB 37.75 21.32 -122.45 -157.83)


;Entfernung von der Osterinsel nach Lima:
(write "Die Distanz von der Osterinsel nach Lima in km beträgt: ")
(distanzAB -27.1 -12.1 -109.4 -77.05)


;;;;;;;;;;;;;;;;;
;; AUFGABE 2.2 ;;
;;;;;;;;;;;;;;;;;


; Richtung des Ziels.
(define deg (degAB latO latH longO longH))

(define (direction latA latB deg)
  (acos (/ (- (sin (deg2rad latB)) (* deg (sin (deg2rad latA)))) (* (cos (deg2rad latA)) (sin (acos deg))))))
  

;;;;;;;;;;;;;;;;;
;; AUFGABE 2.3 ;;
;;;;;;;;;;;;;;;;;

; Liste aller 16 Himmelsrichtungen, sortiert im Uhrzeigersinn
(define directions 
  (list "N" "NNE" "NE" "ENE" "E" "ESE" "SE" "SSE" "S" "SSW" "SW" "WSW" "W" "WNW" "NW" "NNW"))

; 2.3.1 
; Konvertiert einen Richtungswinkel in eine Himmelsrichtung 
;
; Argumente:
;  arc - Der Richtungswinkel in Grad, wobei 0 Grad dem Winkel nach Norden entspricht
;
; Beispielaufrufe:
; > (arc2direction 0)
; > "N"
; > (arc2direction 285.0)
; > "WNW"

(define (arc2direction arc) 
  (list-ref 
   ; Liste aller 16 Himmelsrichtungen, sortiert im Uhrzeigersinn
   directions
   ; Berechnen des Listenindex: Winkel (0..360 Grad) umrechnen auf Listenindex (0..15)
   (inexact->exact ; Index muss exakter Integer sein (zB 4.0 -> 4)
    (modulo ; auch grosse Winkel auf Listenindex abbilden
     (round ; Index ist immer Integer
      (* (/ arc 360) 16)) ; (index = Winkel / 360 ) * 16
     16) ; mod 16
    )
   )
  )

; Hilfsfunktion. Scheint es in Racket's Standardbibliothek nicht zu geben?
; 
; Gibt den Index eines elements in einer Liste zurueck
(define (index-of lst element)
  (if (eq? element (list-ref lst 0)) 
      0  ;Erstes Element, also index = 0
      (+ 1 (index-of (rest lst) element)) ; sonst: In der Restliste gucken
  ) ; if 
) ; define

; 2.3.2
; Konvertiert eine Himmelsrichtung in einen Richtungswinkel (in Grad)
;
; Beispielaufruf:
; > (direction2arc "W")
; > 270
(define (direction2arc direction)
  
  ; Index der Richtung in Liste bestimmen
  (let ([index (index-of directions direction)])
    
    ; Winkel = index * 360 / 16
    (* index (/ 360 16))
    
  ) ; let
 ) ; define