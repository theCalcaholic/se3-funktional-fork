#lang racket

;(eq? (list 1 2 3) (cons 1 (cons 2 (cons 3 '()))))
;(equal? (list 1 2 3) (cons 1 (cons 2 (cons 3 '()))))


; ZUSATZAUFGABE: Ein abstrakter Datentyp für Längenangaben

; a) Konstruktor
; > (make-length 3 'cm)
; '(3 . cm)
(define (make-length value unit)
  (cons value (list unit)))

; b) Selektoren
(define (value-of-length len) (car len))
(define (unit-of-length len) (cadr len))

; c) Skalierung
(define (scale-length len fac)
  (make-length (* fac (value-of-length len)) (unit-of-length len)))

(define *conversiontable* ;
      '( ;  (unit . factor)
         (m . 1)
         (cm . 0.01)
         (mm . 0.001)
         (km . 1000)
         (inch . 0.0254)
         (feet . 0.3048)
         (yard . 0.9144)))

; Umrechnungsfaktor auslesen
(define (factor unit) 
  (cdr (assoc unit *conversiontable*)))

; Normalisierung
; > (length->meter (make-length 2 'mm))
; '(0.002 . m)
(define (length->meter len) 
  (let* ((unit (unit-of-length len))
        (fac (factor unit))
        (value-old (value-of-length len))
        (value-new (* fac value-old)))
    (make-length value-new 'm)
    ))

; General operation for any given operator
(define (oplength cmp len1 len2)
  (apply cmp (list (value-of-length (length->meter len1)) 
     (value-of-length (length->meter len2)))
  )
)

; Compare equal length
; > (length= (make-length 300 'cm) (make-length 3  'm))
; #t
(define (length= len1 len2)
  ((curry oplength =) len1 len2))

; Compare smaller 
; > (length< (make-length 300 'cm) (make-length 32  'm))
; #t
(define (length< len1 len2)
  ((curry oplength <) len1 len2))

; Compare larger
; > (length> (make-length 300 'cm) (make-length 32  'm))
; #f
(define (length> len1 len2)
  ((curry oplength >) len1 len2))

; Addition 
; > (length+ '(3 cm) '(1 km))
; '(1000.03 m)
(define (length+ len1 len2)
  (make-length ((curry oplength +) len1 len2) 'm))

; Subtraktion
; > (length- '(3 km) '(1 inch))
; '(2999.9746 m)
(define (length- len1 len2)
  (make-length ((curry oplength -) len1 len2) 'm))

;;; Teilaufgabe e

; Liste von Längenangaben
(define lens '((6 km) (2 feet) (1 cm) (3 inch)))
lens

; Die Abbildung der Liste auf eine Liste, die die Längenangaben in Metern enthaelt
(map length->meter lens)

; eine Liste aller Längen, die kürzer als 1m sind,
(filter (curry length> '(1 m)) 
        (map length->meter lens))

; die Summe der Längenangaben in Metern
(foldr length+ 
       '(0 m)
       (map length->meter lens))

; die Längenangabe mit der kürzesten Länge in der Liste.
(make-length (apply min (map value-of-length (map length->meter lens)))
             'm)