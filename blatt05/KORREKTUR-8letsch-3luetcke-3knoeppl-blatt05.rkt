#lang racket

;KOMMENTAR: 36 / 36 Punkte

; For visual output of butterflies
(require se3-bib/butterfly-module-2013)

; HOW TO USE THE MODULE:
; (show-butterfly the_color the_pattern the_feeler-shape the_wing-shape)
; the_color: 'blue, 'yello, 'red
; the_pattern: 'stripes, 'dots, 'stars
; the_feeler-shape: 'curved, 'straight, 'curly
; the_wing-shape: 'hexagon, 'rhomb, 'ellipse

; Aufgabe 1.1: ANALYSE UND GROBENTWURF
;KOMMENTAR: 12 Pkt
; Um einen Schmetterling darzustellen, brauchen wir Datentypen, einen 
; für seinen Genotyp (also die Menge der Allele für den Schmetterling)
; und eine Darstellung für den Phänotyp.
;
; GENOTYP stellen wir dar als Liste von Listen der Allele.
; '((blue yellow) (stars dots) (curved straight) (hexagon ellipse)))
;
; PHÄNOTYP stellen wir dar als Liste der konkreten Ausprägungen
; '(blue dots curved hexagon)

;
; Für die Aufteilung des Programms in Funktionen gehen wir nach der gewünschten
; Benutzerinteraktion und verfeinern von dieser Ebene die Implementierung.
; 
; Für zwei gegebene Eltern-Schmetterlinge sollen eine beliebige Anzahl Kinder
; gemäß der Mendelschen Regeln erzeugt werden. Alle Schmetterlinge sollen 
; grafisch ausgegeben werden.
;
; Eltern sollen gemäß ihrer Genotypen definiert werden können:
; > (define mother '( (blue red) (stripes stars) (curved straight) (hexagon ellipse)))
; > (define father '( (yellow yellow) (dots stars) (straight curly) (rhomb ellipse)))
;
; Der folgende Aufruf soll dann eine Anzahl Kinder erzeugen und alle Schmetterlinge ausgeben:
; > (display-children mother father count)
;
; Diese Funktion greift eine Funktion zurück, die die Kinder erzeugt und eine 
; Liste zurückgibt:
; > (generate-children father mother count)
; 
; Diese Funktion generate-children kann rekursiv den count Parameter abbauen
; und dabei eine Liste aufbauen. Jedes Element kann erzeugt werden durch eine 
; weitere Funktion, deren einzige Aufgabe es ist, ein Kind von 2 Eltern zu erzeugen.
; > (generate-child parent1 parent2)
;
; Das Erzeugen erfolgt gemäß Mendelscher Regeln. Wie ein Schmetterling dann aussieht,
; hängt vom Phänotypen ab, der durch die dominanten Alle folgt. Konkret für die Schmetterlinge 
; kann die Dominanzreihenfolge der Allele definiert werden durch eine Liste, in
; der für alle Merkmale eine Liste mit ihren Ausprägunge ist. Die Ausprägungen
; sind sortiert gemäß ihrer Dominanz. 
; > (define traits ...) ; siehe unten

; Wir haben die Modellierung der Datentypen durch Listen gewählt, da wir uns mit
; Listenoperationen wohlfühlen und Racket sich hierfür natürlich besonders eignet.

; Aufgabe 1.2: IMPLEMENTATION UND ERPROBUNG
;KOMMENTAR: sehr schön! 17 Pkt
; List of possible traits for all phenotype characters, ordered from dominant to recessive
(define traits 
  (list
    '(blue yellow red) ; color
    '(stripes dots stars) ; pattern
    '(curved straight curly) ; feeler shape
    '(hexagon rhomb ellipse) ; wing shape
))

; Helper function
;
; Returns a random list element 
; 
; Call:
; > (random-element '(1 2 3 4 5))
; 4
(define (random-element list)
  (list-ref list (random (length list))))


; Generates a child from two given parents. The traits for all phenotype characters are
; assigned randomly, but according to Mendel's rules
;
; parent1: Genotype of first parent
; parent2: Genotype of second parent
; Returns: Genotype of child
;
; Call:
; > (generate-child '((blue red) (stripes stars) (curved straight) (hexagon ellipse)) '((yellow yellow) (dots stars) (straight curly) (rhomb ellipse)))
; '((blue yellow) (stars dots) (curved straight) (hexagon ellipse))
(define (generate-child parent1 parent2)
  ( list
    (list (random-element (car parent1)) (random-element (car parent2)) ) ; color
    (list (random-element (second parent1)) (random-element (second parent2)) ) ; pattern
    (list (random-element (third parent1)) (random-element (third parent2)) ) ; feeler shape   
    (list (random-element (fourth parent1)) (random-element (fourth parent2)) ) ; wing shape  
))

; Generates a given number of butterflies based on 2 parents.
;
; parent1: The genotype of the first parent, given as a list of lists
; parent2: The genotype of the first parent, given as a list of lists
; count:   How many children to generate, given as integer
;
; Call:
; > (generate-children father mother 2)
; '(((yellow red) (stars stars) (straight curved) (rhomb ellipse))
;  ((yellow red) (stars stripes) (straight curved) (ellipse ellipse)))
;
(define (generate-children parent1 parent2 count)
  (if (<= count 0) '()
      (cons (generate-child parent1 parent2) (generate-children parent1 parent2 (- count 1)))
  )
)

; Returns the dominant one of two given traits.
;
; traits: List of 2 traits, out of which to return the dominant one
; domincance: List of traits, ordered from dominant to recessive
;
; Call:
; > (find-dominant '(blue red) '(blue yellow red))
; 'blue
(define (find-dominant traits dominance)
  (let ([trait1 (first traits)] [trait2 (second traits)])
    (if (empty? dominance) #f
      (if (eq? trait1 (car dominance)) trait1
        (if (eq? trait2 (car dominance)) trait2
          (find-dominant traits (cdr dominance))
        )
      ) ; if
    ); if
  ); let
)

; Returns the phenotype of a butterfly from a given genotype
;
; genotype: The butterfly's genotype, given as a list of all characteristics
;
; Call: 
; > (phenotype '((blue yellow) (stars dots) (curved straight) (hexagon ellipse)))
; '(blue dots curved hexagon)
(define (phenotype genotype) 
  (list 
   (find-dominant (first genotype) (first traits))
   (find-dominant (second genotype) (second traits))
   (find-dominant (third genotype) (third traits))
   (find-dominant (fourth genotype) (fourth traits))
   )
  )


; Generates and displays a given number of butterflies
; who are descendants of a given pair of parents.
;
; parent1: The genotype of the first parent, given as a list of lists
; parent2: The genotype of the first parent, given as a list of lists
; count:   How many children to generate and display, given as integer
;
; Call:
; > (display-children mother father 5)
; (list ... ... ...) ; display images of the two parents and 5 children
(define (display-children parent1 parent2 count)
  (map (lambda (butterfly) (apply show-butterfly (phenotype butterfly)))
       (append (list parent1 parent2) (generate-children parent1 parent2 count))
  ) ; map
) ; define

; ERPROBUNG

; BEISPIEL aus der Aufgabenstellung
;KOMMENTAR: star ohne s hinten!
; two butterflies defined by their genotype
(define mother '( (blue red) (stripes stars) (curved straight) (hexagon ellipse))) 
(define father '( (yellow yellow) (dots stars) (straight curly) (rhomb ellipse)))
; Uncomment the following line to show 5 children who are possible descendants of the defined butterflies
 (display-children mother father 5)
; Es werden tatsächlich nur Kinder erzeugt, die aus den Genotypen der Eltern entstanden
; sein können. Die Phänotypen zeigen nur dominanten Ausprägungen.

; WEITERE TESTDATEN mit nur rezessiven Ausprägungen auf einer Seite

(define mother2 '( (red red) (stars stars) (curved curved) (ellipse ellipse)))
; > (generate-children mother2 father 5)
; Erzeugt Kinder: 
;'(((red yellow) (stars stars) (curved straight) (ellipse ellipse))
;  ((red yellow) (stars stars) (curved curly) (ellipse rhomb))
;  ((red yellow) (stars dots) (curved straight) (ellipse rhomb))
;  ((red yellow) (stars stars) (curved straight) (ellipse rhomb))
;  ((red yellow) (stars stars) (curved straight) (ellipse ellipse)))
; Die Phänotypen zeigen wie erwartet nur Ausprägungen von "father", da dieser die 
; dominanten Allele geliefert hat.

; > (display-children mother2 mother2 5)
; Liefert 5 identische Kinder die identisch zu beiden Eltern sind. 
; Dies ist wie erwartet, da es nur eine Ausprägung in der
; gesamten Vererbung gibt.


; Aufgabe 2: FARBMUTATIONEN

; siehe andere Datei