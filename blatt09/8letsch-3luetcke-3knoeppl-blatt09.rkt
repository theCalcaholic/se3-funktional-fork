#lang swindle
(require swindle/setf swindle/misc)

;;; AUFGABE 1: CLOS UND GENERISCHE FUNKTIONEN ;;;

; 1.1 DEFINITION VON KLASSEN

(defclass publikation ()
  ; slots
  (key)
  (autor :initvalue 'Unbekannt
         :reader pub-autor
         :initarg :autor)
  (jahr :initvalue 0
        :initarg :jahr
        :reader pub-jahr)
  (titel :initvalue 'Unbekannt
         :initarg :titel
         :reader pub-titel)
  
  ; class options
  :printer #t
  )

(defclass buch (publikation)
  (verlag :initarg :verlag 
          :reader pub-verlag)
  (ort :initarg :ort
       :reader pub-ort)
  (reihe :initarg :reihe
         :reader pub-reihe)
  (seriennummer :initarg :seriennummer
                :reader pub-seriennummer)
)

(defclass sammelband (buch)
  (herausgeber :initarg :herausgeber
               :reader pub-herausgeber)
  (seiten :initarg :seiten
          :reader pub-seiten)
  )

(defclass artikel (publikation)
  (zeitschrift :initarg :zeitschrift
               :reader pub-zeitschrift)
  (bandnummer :initarg :bandnummer
              :reader pub-bandnummer)
  (heftnummer :initarg :heftnummer
              :reader pub-heftnummer)
  (monat :initarg :monat
         :reader pub-monat)
  )

; Beispiel 1: Ein Buch
(define Nessie1790 (make buch 
      :autor "Nessie"
      :jahr 1790
      :titel "Mein Leben In Loch Ness - Verfolgt als Ungeheuer"
      :verlag "Minority-Verlag"
      :ort "Inverness"
      :reihe "Die Besondere Biographie"
      :seriennummer 2))

; Beispiel 2: Ein Sammelband
(define Prefect1979 (make sammelband
      :autor "Prefect, F."
      :jahr 1979
      :titel "Mostly Harmless - Some Observations Concerning The Third Planet Of The Solar System"
      :verlag "Galactic Press"
      :ort "Vega-System, 3rd Panet"
      :reihe "Travel In Style"
      :seriennummer 5
      :herausgeber "Adams, D., editor, The Hitchhiker's Guide to the Galaxy"
      :seiten "1337 Edition, p. 420"))

; Beispiel 3: Ein Zeitschriftenartikel

(define Wells3200 (make artikel
      :autor "Wells, H. G."
      :jahr 3200
      :titel "Zeitmaschinen leicht gemacht"
      :zeitschrift "Heimwerkerpraxis für Anfänger"
      :bandnummer 3
      :heftnummer 550))
      

; 1.2 GENERISCHE FUNKTIONEN UND METHODEN

; Druckt ein Zitat aus. Generische Operation, definiert noch keine Methode
(defgeneric* cite ((p publikation)))

; Methodenimplementierung für allgemeinen Typen "publikation"
(defmethod cite ((p publikation))
  (printf "~a (~s). ~a."
           (pub-autor p) (pub-jahr p) (pub-titel p)
  ))

; Methodenimplementierung für Typen "buch"
; > (cite Nessie1790)
; Nessie (1790). Mein Leben In Loch Ness - Verfolgt als Ungeheuer
; Band 2 der Reihe: Die Besondere Biographie. Minority-Verlag, Inverness.
(defmethod cite ((b buch))
  (printf "~a (~s). ~a\nBand ~a der Reihe: ~a. ~a, ~a."
           (pub-autor b) (pub-jahr b) (pub-titel b) 
           (pub-seriennummer b) (pub-reihe b) (pub-verlag b) (pub-ort b)
  ))

; Methodenimplementierung für Typ "sammelband"
; > (cite Prefect1979)
; Prefect, F. (1979). Mostly Harmless - Some Observations Concerning The Third Planet Of The Solar System
; In Adams, D., editor, The Hitchhiker's Guide to the Galaxy, volume 5 of "Travel In Style". Galactic Press, Vega-System, 3rd Panet, 1337 Edition, p. 420.
(defmethod cite ((b sammelband))
  (printf "~a (~s). ~a\nIn ~a, volume ~a of ~s. ~a, ~a, ~a."
           (pub-autor b) (pub-jahr b) (pub-titel b) 
           (pub-herausgeber b) (pub-seriennummer b) (pub-reihe b)
           (pub-verlag b) (pub-ort b) (pub-seiten b)
  ))

; Methodenimplementierung für Typ "artikel"
; > (cite Wells3200)
; Wells, H. G. (3200). Zeitmaschinen leicht gemacht. Heimwerkerpraxis für Anfänger, 550(3).
(defmethod cite ((a artikel))
  (printf "~a (~s). ~a. ~a, ~a(~a)."
           (pub-autor a) (pub-jahr a) (pub-titel a) 
           (pub-zeitschrift a) (pub-heftnummer a) (pub-bandnummer a)
  ))

; 1.3 ERGÄNZUNGSMETHODEN

; Ergänzungsmethoden sind eine Möglichkeit in CLOS, um Funktionalität aus der vererbten
; Klasse durch weitere Funktionalität zu erweitern.

; Die primäre Methode einer Oberklasse wird hierbei durch Hilfsmethoden ergänzt, 
; die vorher oder nachher (oder beides) zusätzlich auszuführen sind.
; Es gibt Vormethoden, Nachmethoden oder einhüllende Methoden, 
; erkenntlich durch die Schlüsselwörter :before, :around, :after.

; Ergänzungsmethoden sind eine Alternative zu einem super call (wie zB in Java).
; Im Gegensatz zum super call ist bei Ergänzungsmethoden sichergestellt, daß alle 
; Ergänzungsmethoden ausgeführt werden. So können keine Initialisierungen vergessen 
; oder unterdrückt werden, die in den Oberklassen definiert wurden.

; In unserem Fall wäre es möglich, dass die Oberklasse "publikation" eine Methode
; cite definiert und implementiert. Diese würde die Mindestmenge an Slots für die 
; Signatur ausgeben: Autor, Jahr und Titel.
; Erbende Klassen würden dann Nachmethoden definieren, die jeweils ihre zusätzlichen
; Informationen als Ausgabe anhängen.
; Dies funktioniert natürlich nur, wenn der zu generierende Text eine reine
; Erweiterung ist und keine bestehenden Texte überschrieben werden müssen.

;;; AUFGABE 2: CLOS UND VERERBUNG ;;;

; 2.1 DEFINITION VON KLASSEN

; Allgemeines Fahrzeug (Wurzel der Vererbungshierachie)
(defclass Fahrzeug ())

; Einzweckfahrzeuge
(defclass Landfahrzeug (Fahrzeug))
(defclass Schienenfahrzeug (Landfahrzeug))
(defclass Straßenfahrzeug (Landfahrzeug))
(defclass Wasserfahrzeug (Fahrzeug))
(defclass Luftfahrzeug (Fahrzeug))

; Mehrzweckfahrzeuge
(defclass Amphibienfahrzeug (Wasserfahrzeug Landfahrzeug))
(defclass Amphibienflugzeug (Straßenfahrzeug Wasserfahrzeug Luftfahrzeug))
(defclass Zweiwegefahrzeug (Straßenfahrzeug Schienenfahrzeug))
(defclass Zeitzug (Schienenfahrzeug Luftfahrzeug))

; 2.2 OPERATIONEN UND METHODENKOMBINATION

; Alle diese Operationen sind einfache Getter zum Abfragen bestimmter Werte. 
; Diese Werte soll jedes Fahrzeug liefern können,egal um welches konkrete
; Fahrzeug aus der Vererbungshierarchie es sich handelt. Daher wird als
; einziger Parameter nur ein Fahrzeug f erwartet.
; Je nach konkretem Typ muss der Wert aber zusammengesetzt werden aus den vererbten
; Eigenschaften. So hat ein Amphibienfahrzeug etwa einen unterschiedlichen
; Verbrauch auf Land als auf Wasser. Das Ergebnis muss eine Kombination dieser
; unterschiedlichen Werte sein.

; Abfrage des Mediums, in dem sich das Fahrzeug bewegt
; Gibt eine Liste zurück, die alle Medien enthält von allen Typen, die 
; dieses Fahrzeug erweitert (keine Duplikate)
(defgeneric fahrzeug-medium (f Fahrzeug))

; Abfrage der Maximalgeschwindigkeit
; Gibt einen einzelnen Wert zurück. Dieser ist der größte Wert 
; aller vererbten Fahrzeugtypen.
;(defgeneric fahrzeug-maxspeed (f Fahrzeug))

; Abfrage der Zuladung (Tragfähigkeit)
; Gibt einen einzelnen Wert aus. Dieser ist der kleinste aller Werte der
; vererbten Fahrzeugtypen.
(defgeneric fahrzeug-zuladung (f Fahrzeug))

; Abfrage des Verbrauchs pro 100 km
; Gibt einen einzelnen Wert aus. Dieser ist der Durchschnitt der Werte 
; der vererbten Fahrzeugtypen.
(defgeneric fahrzeug-verbrauch (f Fahrzeug))

; Abfrage der Passagierzahl
; Gibt einen einzelnen Fahrzeugtyp zurück. Dieser ist fest definiert 
; für jede konkrete Klasse. Es gibt da keine schlüssige Vererbungslogik
; um diesen Wert automatisch zu bestimmen.
(defgeneric fahrzeug-passagierzahl (f Fahrzeug))

; 2.3 KLASSENPRÄZEDENZ BEI MEHRFACHVERERBUNG

(defgeneric maxspeed ((f Fahrzeug))
  :combination generic-max-combination)

(defmethod maxspeed ((f Fahrzeug)) 0)
(defmethod maxspeed ((f Landfahrzeug)) 220)
(defmethod maxspeed ((f Schienenfahrzeug)) 150)
(defmethod maxspeed ((f Straßenfahrzeug)) 220)
(defmethod maxspeed ((f Wasserfahrzeug)) 80)
(defmethod maxspeed ((f Luftfahrzeug)) 400)

; Beispiel Einzweckfahrzeug
(define flugzeug (make Luftfahrzeug))
; > (maxspeed flugzeug)
; 400
; works!

; Beispiele Mehrzweckfahrzeuge
(define amphibienfahrzeug (make Amphibienfahrzeug))
(define amphibienflugzeug (make Amphibienflugzeug))
(define zweiwegefahrzeug (make Zweiwegefahrzeug))
(define zeitzug (make Zeitzug))

; > (get-maxspeed amphibienfahrzeug)
; 220
; > (maxspeed amphibienflugzeug)
; 400
; > (maxspeed zweiwegefahrzeug)
; 220
; > (maxspeed zeitzug)
; 400

; Erklärung:
; Ist für eine Klasse keine konkrete Methode implementiert, wird die 
; vererbte generische Version genommen. In unserem Fall passiert dies 
; bei den Mehrzweckfahrzeugen.
; Die generische Methode ist so definiert, dass eine Methodenkombination
; aus den entsprechenden Methoden der Elternklassen berechnet wird 
; (in unserem Fall mit dem maxspeed wird das Maximum berechnet).

; Hier ist es wichtig, die Klassenpräzedenz zu verstehen. 
; Diese regelt in CLOS, was bei Vererbungskonflikten geschehen soll.
; So hat jede Klasse Vorrang vor ihren Oberklassen. Außerdem
; legt jede Klasse die Präzedenz der direkten Oberklassen fest
; (bei Mehrfachvererbung).

; In unserem Beispiel heißt dies, das direkt implementierte Methoden
; Vorrang vor den Oberklassen haben. Dies wird deutlich bei den Einzweckfahrzeugen.
; Bei Mehrzweckfahrzeugen dagegen werden die Methoden der Oberklassen in der Reihenfolge
; entsprechend des Präzedenzgraphen aufgerufen. Im Beispiel der Methodenkombination mit "combine-max-generic"
; ist die exakte Reihenfolge egal, da diese Operation kommutativ ist.
